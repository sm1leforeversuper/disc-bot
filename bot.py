import discord
from discord.ext import commands

# =command
prefix = '='
client = commands.Bot(command_prefix = prefix)
client.remove_command('help')


@client.event
async def on_ready():
    print('BOT connected')

# Clear command
@client.command(pass_context = True)
async def hello(ctx, amount = 1):
    await ctx.channel.purge(limit = amount)

    author = ctx.message.author
    await ctx.send(f'Здарова, {author.mention}')


# Clear messages
@client.command(pass_context = True)
@commands.has_any_role('уверенные', 'сброд', 'пропускной')
async def clear(ctx, amount = 1000):
    await ctx.channel.purge(limit = amount)

# Kick
@client.command(pass_context = True)
@commands.has_any_role('уверенные', 'сброд', 'пропускной')
async def kick(ctx, member: discord.Member, *, reason = None):
    emb = discord.Embed(title = 'ннныа в ебучку', color = discord.Color.orange())
    await ctx.channel.purge(limit = 1)
    await member.kick(reason = reason)
    await ctx.send(f'отдохни {member.mention}')
    emb.set_author(name = member.name, icon_url = member.avatar_url)
    emb.add_field(name = 'получила дура', value = 'потом вернется: {}'.format(member.mention))
    await ctx.send(embed = emb)

# Ban
@client.command(pass_context = True)
@commands.has_role('сброд')
async def ban(ctx, member: discord.Member, *, reason = None):
    emb = discord.Embed(title = 'В помойку', color = discord.Color.red())
    await ctx.channel.purge(limit = 1)
    await member.ban(reason = reason)
    emb.set_author(name = member.name, icon_url = member.avatar_url)
    emb.add_field(name = 'отлетает', value = 'нхауй с пляжа: {}'.format(member.mention))
    await ctx.send(embed = emb)

# Help
@client.command(pass_context = True)
async def help(ctx):
    emb = discord.Embed(title = 'вот че', color = discord.Color.dark_blue())
    emb.add_field(name = '{}clear'.format(prefix), value = 'удалить до касаря сообщений(от уверенных и выше)')
    emb.add_field(name = '{}kick'.format(prefix), value = 'кик лоха (от уверенных и выше)')
    emb.add_field(name = '{}ban'.format(prefix), value = 'забанить чмошника(тока для админов)')
    emb.set_author(name = client.user.name, icon_url = client.user.avatar_url)
    emb.set_footer(text = f'{ctx.author.name}, ну вот и все', icon_url = ctx.author.avatar_url)
    
    await ctx.send (embed = emb)

@client.command(pass_context = True)
async def test(ctx):
    emb = discord.Embed(title = 'TEST', color = discord.Color.blue())

    await ctx.send (embed = emb)



# Connect
token = open('token.txt', 'r').readline()

client.run(token)